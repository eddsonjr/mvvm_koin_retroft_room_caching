# Caching With Room

O CachingWithRoom é uma aplicação de exemplos escrita em Kotlin cujo objetivo é 
adquirir uma lista de usuários do servidor, mostrá-la na tela por fim e salvá-la em cache na aplicação via Room.

## Tecnologias e padrões usados
 - Arquitetura MVVM (View - View Model - Repository - DataSource)
 - Room 
 - Coroutines
 - Retrofit / OKHttp
 - ViewState
 - Koin
 - SwipeRefresh
 - SaveInstance   
 - ViewBinding


## Branchs

Este repositório contem as seguintes branchs: 

 - __Main / CachingWithRoom__: Projeto com retrofit e cache 
 - __OnlyRemoteData__: Aplicação somente adquirindo dados da web via retrofit, sem cache.


## Json - Response

A aplicação consome um json contendo uma pequena lista de informações de usuários. O Json apresenta o seguinte formato e informações: 

```

[
  {
    "id": 1,
    "name": "Ana Steam",
    "phone": "+55 92 997601234",
    "address": " Av. São Paulo, S/N, São Paulo"
  },
  {
    "id": 2,
    "name": "Sarah Lins",
    "phone": "+55 41 987460099",
    "address": " Rua Teoodoro Fonseca, 54, São Paulo"
  },
  {
    "id": 3,
    "name": "Aly Dork",
    "phone": "+911 1  487460099",
    "address": " Austin Street, 65, Austin"
  },
  {
    "id": 4,
    "name": "July  Dork",
    "phone": "+911 1  487460077",
    "address": " Austin Street, 9, Austin"
  },
  {
    "id": 5,
    "name": "Olesia Dautov",
    "phone": "+788 99 127695734",
    "address": "Dimitry Street, 98, Samara Oblast"
  }
]


```

## Arquitetura da Aplicação - Cache com Room

A aplicação faz uso da Arquitetura MVVM comtemplando as seguintes camadas: UI / View Model / Repository / DataSource.   
  
Especificamente quando se trata de cache dos dados com Room, a arquitetura passa a apresentar dois DataSources - um responsável pela aquisição dos dados via web e outro pelo gerenciamento do banco de dados - e dois Repositories, sendo um responsável por comunicar com a camada de datasource remoto e outro com a camada de datasource local. 

No entanto, na camada de Repository, há uma classe _Repository_ responsável por permitir acesso ao local ou ao remoto: 

```
class Repository(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository) {

    val remote = remoteRepository
    val local = localRepository
}

```

Já no View Model, é injetado esta classe _Repository_, permitindo escolher quando usar o repository local ou o remoto:

```
    class UserViewModel(private val repository: Repository,private val context: Context): ViewModel() {

    //... repository.local.getUsers()
    //... repository.remote.getUsers()


}

``` 
![plot](/uploads/447100037ed990ae67b48050c7f2f4ae/caching_with_room_diagram.jpeg)




### Sem cache

Caso você acabe optando por clonar a branch _OnlyRemoteData_, a arqutietura segue o padrão MVVM convencional, tendo apenas um  DataSource e um Repository. 












