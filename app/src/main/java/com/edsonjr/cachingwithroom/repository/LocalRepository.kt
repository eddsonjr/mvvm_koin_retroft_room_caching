package com.edsonjr.cachingwithroom.repository

import android.util.Log
import com.edsonjr.cachingwithroom.datasource.local.DAO
import com.edsonjr.cachingwithroom.datasource.local.entities.UserEntity
import com.edsonjr.cachingwithroom.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LocalRepository(private val localDataSource: DAO) {

    private val TAG = this.javaClass.name

    suspend fun getUsers(): List<User>? = withContext(Dispatchers.IO) {
        val usersFromDB = localDataSource.getUsers()
        Log.d(TAG,"Data returned from database: $usersFromDB")
        usersFromDB
    }

    suspend fun saveUser(users: List<User>) {
        Log.d(TAG, "Saving data into local db")
        users.forEach {
            Log.d(TAG,"Saving $it ...")
            localDataSource.insertUsers(mapToEntity(it))
        }
    }

    suspend fun deleteAllUsersFromLocal() {
        Log.d(TAG,"Deleting all users from local db")
        localDataSource.deleteAllUsersFromDatabase()
    }




    private fun mapToEntity(user: User): UserEntity {
        return UserEntity(
            id = user.id,
            name = user.name,
            phone = user.phone,
            address = user.address
        )
    }
}