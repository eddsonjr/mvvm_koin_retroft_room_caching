package com.edsonjr.cachingwithroom.repository

class Repository(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository) {

    val remote = remoteRepository
    val local = localRepository
}