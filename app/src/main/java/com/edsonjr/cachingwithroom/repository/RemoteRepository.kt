package com.edsonjr.cachingwithroom.repository

import android.util.Log
import com.edsonjr.cachingwithroom.datasource.remote.RemoteDataSource
import com.edsonjr.cachingwithroom.model.User
import com.edsonjr.cachingwithroom.service.BaseApiResponse
import com.edsonjr.cachingwithroom.service.NetworkResult

class RemoteRepository(private val remoteDataSource: RemoteDataSource): BaseApiResponse() {

    private val TAG = this.javaClass.name

    suspend fun getUsers(): NetworkResult<List<User>> {
        Log.d(TAG,"Getting user data from service")
        return safeApiCall { remoteDataSource.getUsers() }
    }
}