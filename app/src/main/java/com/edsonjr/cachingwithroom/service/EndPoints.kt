package com.edsonjr.cachingwithroom.service


import com.edsonjr.cachingwithroom.model.User
import retrofit2.Response

import retrofit2.http.GET

interface EndPoints {

    @GET("DZMM")
    suspend fun getUsers(): Response<List<User>>

}