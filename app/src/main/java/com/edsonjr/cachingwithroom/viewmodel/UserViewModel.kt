package com.edsonjr.cachingwithroom.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edsonjr.cachingwithroom.datasource.local.entities.UserEntity
import com.edsonjr.cachingwithroom.model.User
import com.edsonjr.cachingwithroom.repository.RemoteRepository
import com.edsonjr.cachingwithroom.repository.Repository
import com.edsonjr.cachingwithroom.service.NetworkResult
import com.edsonjr.cachingwithroom.utils.NetworkUtils
import kotlinx.coroutines.launch

class UserViewModel(private val repository: Repository,private val context: Context): ViewModel() {

    private val TAG = this.javaClass.name

    private val _mainState = MutableLiveData<ViewState<List<User>>>()
    val mainState get() = _mainState


    fun getUsers() = viewModelScope.launch {
        if(NetworkUtils.isNetworkAvailable(context)){
            Log.d(TAG,"Internet connection available. Calling for remote data...")
            getRemoteUsers()


        }else{
            Log.d(TAG,"Internet connection unavailable. Calling for local data...")
            getLocalUsers()
        }
    }




    //region: LOCAL
    private fun getLocalUsers() = viewModelScope.launch {
        _mainState.postValue(ViewState.LOADING)

        val localData: List<User>? = repository.local.getUsers()

        if(localData?.isNotEmpty() == true){
            Log.d(TAG,"Get users from database returned successfully")
            Log.d(TAG,"data: $localData")
            _mainState.postValue(ViewState.SUCCESS(localData))
        }else{

            if(localData.isNullOrEmpty()){
                Log.d(TAG,"Empty datbase")
                _mainState.postValue(ViewState.EMPTY)
            }else {
                Log.e(TAG,"Error when get local data or empty")
                _mainState.postValue(ViewState.ERROR)
            }
        }
    }


    private fun offlineCacheUsers(users: List<User>) = viewModelScope.launch {
        repository.local.deleteAllUsersFromLocal()
        repository.local.saveUser(users)
    }

    //endregion



    //region: REMOTE
    private fun getRemoteUsers() = viewModelScope.launch {
        _mainState.postValue(ViewState.LOADING)

        val remoteRequest = repository.remote.getUsers()

        when(remoteRequest){
            is NetworkResult.Success -> {
                val data = remoteRequest.data
                Log.d(TAG,"Get users service returned successfully")
                Log.d(TAG,"data: $data")
                _mainState.postValue(ViewState.SUCCESS(data))

                //caching (save into room)
                if (data != null) {
                    offlineCacheUsers(data)
                }
            }
            is NetworkResult.Error -> {
                val error = remoteRequest.errorMsg
                Log.e(TAG,"Error when get users from service")
                Log.e(TAG,error.toString())
                _mainState.postValue(ViewState.ERROR)
            }
        }
    }

    //endregion
}