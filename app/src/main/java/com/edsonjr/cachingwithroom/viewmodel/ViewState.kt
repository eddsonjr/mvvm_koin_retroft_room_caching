package com.edsonjr.cachingwithroom.viewmodel

sealed class ViewState<out T> {
    data class SUCCESS<T> (val data: T?): ViewState<T>()
    object ERROR: ViewState<Nothing>()
    object LOADING: ViewState<Nothing>()
    object EMPTY: ViewState<Nothing>()
}
