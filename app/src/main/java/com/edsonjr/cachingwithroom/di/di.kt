package com.edsonjr.cachingwithroom.di

import android.app.Application
import androidx.room.Room
import com.edsonjr.cachingwithroom.constants.AppConstants.Companion.DATABASE_NAME
import com.edsonjr.cachingwithroom.datasource.local.DAO
import com.edsonjr.cachingwithroom.datasource.local.DataBase
import com.edsonjr.cachingwithroom.datasource.remote.RemoteDataSource
import com.edsonjr.cachingwithroom.repository.LocalRepository
import com.edsonjr.cachingwithroom.repository.RemoteRepository
import com.edsonjr.cachingwithroom.repository.Repository
import com.edsonjr.cachingwithroom.service.EndPoints
import com.edsonjr.cachingwithroom.service.RetrofitService
import com.edsonjr.cachingwithroom.viewmodel.UserViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit

object di {

    private val retrofitModule = module {
        single { RetrofitService().initRetrofit() }
        single<EndPoints>{get<Retrofit>().create(EndPoints::class.java)}
    }


    private val viewModelModule = module {
        single{ UserViewModel(get(),get())}
    }


    private val repositoryModule = module {
        factory { RemoteRepository(get()) }
        factory { LocalRepository(get()) }
        factory { Repository(get(),get()) }
    }

    private val dataSourceModule = module {
        factory { RemoteDataSource(get()) }
        single { DataBase.getInstance(androidContext()).usersDAO }
    }


    val appModules = listOf(
        retrofitModule,
        viewModelModule,
        repositoryModule,
        dataSourceModule
    )

}