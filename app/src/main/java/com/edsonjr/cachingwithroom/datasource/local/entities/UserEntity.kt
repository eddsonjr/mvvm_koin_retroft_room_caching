package com.edsonjr.cachingwithroom.datasource.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.edsonjr.cachingwithroom.constants.AppConstants.Companion.DATABASE_TABLE_NAME


@Entity(tableName = DATABASE_TABLE_NAME)
data class UserEntity(

    @PrimaryKey(autoGenerate = false)
    var id: Int,

    @ColumnInfo(name = "name", typeAffinity = ColumnInfo.TEXT)
    var name: String,

    @ColumnInfo(name = "phone", typeAffinity = ColumnInfo.TEXT)
    var phone: String,

    @ColumnInfo(name = "address", typeAffinity = ColumnInfo.TEXT)
    var address: String,
)