package com.edsonjr.cachingwithroom.datasource.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.edsonjr.cachingwithroom.datasource.local.entities.UserEntity
import com.edsonjr.cachingwithroom.model.User


@Dao
interface DAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUsers(userEntity: UserEntity)

    @Query("DELETE FROM user_table")
    suspend fun deleteAllUsersFromDatabase()

    @Query("SELECT * FROM user_table ORDER BY id ASC")
    fun getUsers(): List<User>

}