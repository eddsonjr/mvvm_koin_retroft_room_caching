package com.edsonjr.cachingwithroom.datasource.remote

import com.edsonjr.cachingwithroom.service.EndPoints

class RemoteDataSource(private val api: EndPoints) {

    suspend fun getUsers() = api.getUsers()
}