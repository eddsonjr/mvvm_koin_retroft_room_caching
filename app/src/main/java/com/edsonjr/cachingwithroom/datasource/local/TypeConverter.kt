package com.edsonjr.cachingwithroom.datasource.local

import androidx.room.TypeConverter
import com.edsonjr.cachingwithroom.model.User
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class TypeConverter {

    var gson = Gson()

    @TypeConverter
    fun convertUsersToString(users: List<User>): String {
        return gson.toJson(users)
    }

    @TypeConverter
    fun convertStringToListOfUsers(str: String): List<User> {
        val listType = object  : TypeToken<List<User>>(){}.type
        return gson.fromJson(str,listType)
    }


}