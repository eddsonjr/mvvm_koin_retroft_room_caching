package com.edsonjr.cachingwithroom.datasource.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.edsonjr.cachingwithroom.constants.AppConstants.Companion.DATABASE_NAME
import com.edsonjr.cachingwithroom.datasource.local.entities.UserEntity


@Database(
    entities = [UserEntity::class],
    version = 1,
    exportSchema = false

)
@TypeConverters(TypeConverter::class)
abstract class DataBase: RoomDatabase() {

    abstract val usersDAO: DAO

    companion object {

        @Volatile
        private var INSTANCE: DataBase? = null

        fun getInstance(context: Context): DataBase {
            synchronized(this){
                var instance = INSTANCE
                if(instance == null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        DataBase::class.java,
                        DATABASE_NAME
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}