package com.edsonjr.cachingwithroom.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.cachingwithroom.databinding.ItemUserBinding
import com.edsonjr.cachingwithroom.model.User


class UserAdapter: RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    private var userList: MutableList<User> = mutableListOf()

    inner class UserViewHolder(private val binding: ItemUserBinding):
        RecyclerView.ViewHolder(binding.root){

        fun bind(user: User){
            binding.userName.text = user.name
            binding.userAddress.text = user.address
            binding.userPhone.text = user.phone
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemUserBinding.inflate(layoutInflater)
        return UserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(userList[position])
    }

    override fun getItemCount(): Int = userList.size


    fun updateAdapter(users: List<User>){
        if(this.userList.isNotEmpty()){
            this.userList.clear()

        }
        this.userList = users.toMutableList()
        notifyDataSetChanged()

    }
}