package com.edsonjr.cachingwithroom.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.cachingwithroom.constants.AppConstants.Companion.SAVE_INSTANCE_KEY
import com.edsonjr.cachingwithroom.databinding.ActivityMainBinding
import com.edsonjr.cachingwithroom.model.User
import com.edsonjr.cachingwithroom.viewmodel.UserViewModel
import com.edsonjr.cachingwithroom.viewmodel.ViewState
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.Serializable
import android.widget.Toast
import com.edsonjr.cachingwithroom.R


class MainActivity : AppCompatActivity() {

    private val TAG = this.javaClass.name
    private lateinit var binding: ActivityMainBinding
    private val userViewModel: UserViewModel  by viewModel() 
    private val adapter = UserAdapter()

    private var listOfUsers: List<User>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState != null) {
            Log.d(TAG,"Returning data from saved instance...")
            listOfUsers = savedInstanceState.getSerializable(SAVE_INSTANCE_KEY) as List<User>
            setupUIForSuccess()
        }else {
            //calling viewmodel for getUsers
            userViewModel.getUsers()
        }

        //observe the viewModel get users request
        observeForData()

        //observe refresh
        setupRefreshSwipeListener()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable(SAVE_INSTANCE_KEY,listOfUsers as Serializable)
        super.onSaveInstanceState(outState)
    }




    private fun observeForData() {
        userViewModel.mainState.observe(this){ viewState ->
            when(viewState) {
                is ViewState.LOADING -> {
                    Log.d(TAG, "Loading users...")
                    setupUIForLoading()

                }

                is ViewState.SUCCESS -> {
                    Log.d(TAG,"Get users operation returned successfully")
                    viewState.data?.let {
                        listOfUsers = it
                        setupUIForSuccess()
                    }
                }
                is ViewState.ERROR -> {
                    Log.d(TAG, "Error when get users")
                    setupUIForError()

                }

                is ViewState.EMPTY -> {
                    Log.d(TAG,"Error when get local users")
                    setupUIForEmptyData()
                }
            }
        }
    }

    private fun setupUIForLoading() {
        binding.progressbar.visibility = View.VISIBLE
        binding.errorText.visibility = View.GONE
        binding.recyclerview.visibility = View.GONE
        binding.swipeContainer.isRefreshing = false
    }


    private fun setupUIForError() {
        binding.progressbar.visibility = View.GONE
        binding.errorText.visibility = View.VISIBLE
        binding.recyclerview.visibility = View.GONE
        binding.swipeContainer.isRefreshing = false
    }

    private fun setupUIForEmptyData() {
        binding.progressbar.visibility = View.GONE
        binding.errorText.visibility = View.VISIBLE
        binding.errorText.text = getString(R.string.empty_database_msg)
        binding.recyclerview.visibility = View.GONE
        binding.swipeContainer.isRefreshing = false
    }

    private fun setupUIForSuccess() {
        binding.progressbar.visibility = View.GONE
        binding.errorText.visibility = View.GONE
        binding.recyclerview.visibility = View.VISIBLE
        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(this)
        listOfUsers?.let { adapter.updateAdapter(it) }
        binding.swipeContainer.isRefreshing = false
    }

    private fun setupRefreshSwipeListener() {
        binding.swipeContainer.setOnRefreshListener {
            Log.d(TAG,"Refreshing...")

            //calling viewmodel for getUsers
            userViewModel.getUsers()
        }
    }
}