package com.edsonjr.cachingwithroom.constants

class AppConstants {
    companion object {

        //key used to save instance of activity
        const val SAVE_INSTANCE_KEY = "save_instance_users_key"


        //name of database and table
        const val DATABASE_NAME = "caching_with_room_db"
        const val DATABASE_TABLE_NAME = "user_table"

    }
}